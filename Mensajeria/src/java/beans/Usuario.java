/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author elcer
 */
public class Usuario {
    private final String nombre, apellidos; 
    private String email = "", password = "";
    
    public Usuario(String nombre, String apellidos) {
        this.nombre = nombre;
        this.apellidos = apellidos;
    }
    
    //este constructor solo se usa para loguear al usuario en el sistema
    public Usuario(String nombre, String apellidos, String email, String pw) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        if (email.contains("@") && email.endsWith(".com")) {
            String em1[] = email.split("@");
            String em2[] = em1[1].split(".com");
            if (!em1[0].isEmpty() && !em2[0].isEmpty()) 
                this.email = email;
        } 
        if (pw.length() >= 8) password = pw;
    }
    
    public void setEmail(String email) {
        if (email.contains("@") && email.endsWith(".com")) {
            String em1[] = email.split("@");
            String em2[] = em1[1].split(".com");
            if (!em1[0].isEmpty() && !em2[0].isEmpty()) 
                this.email = email;
        } 
    }
    public void setPassword(String pw1, String pw2) {
        if (pw1.length() >= 8 && pw2.length() >= 8 
                && pw1.equals(pw2)) {
            this.password = pw1;
        }
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public String getApellidos() {
        return apellidos;
    }
    
    public String getEmail() {
        return email;
    }
    
    public String getPassword() {
        return password;
    }
}
