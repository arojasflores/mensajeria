/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author elcer
 */
public class Mensaje {
    private final Usuario origen;
    private final Usuario destino;
    private String texto="";
    
    public Mensaje(Usuario origen, Usuario destino) {
        this.origen = origen;
        this.destino = destino;
    }
    
    public void setCuerpo(String texto) {
        this.texto = texto;
    }
    
    public Usuario getOrigen() {
        return origen;
    }
    
    public Usuario getDestino() {
        return destino;
    }
    
    public String getTexto() {
        return texto;
    }
}
