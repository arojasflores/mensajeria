/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.sql.*;
import java.util.*;

/**
 *
 * @author elcer
 */
public class Servicio {
    private final String url = "jdbc:mysql://localhost:3306/mensajeria";
    private final String driver = "com.mysql.jdbc.Driver"; 
    
    private Usuario usuario = null;
    
    private Connection iniciarConexion() {
        Connection conexion = null;
        try {
            Class.forName(driver);
            conexion = DriverManager.getConnection(url, "root", "");
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage() + 
                    ". >>> Error de Conexion 1!!");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage() + 
                    ". >>> Error de Conexion 2!!");
        }
        return conexion;
    }
    
    private void finalizarConexion(Connection conexion) {
        if (conexion != null) {
            try {
                conexion.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage() + 
                        ". >>> Error de Desconexion!!");
            }
        }
    }
    
    /*
    
    
    
    
    */
 
    public boolean registrarse(String nombre, String apellidos, 
                                String email, String pw1, String pw2) {
        Connection conexion = iniciarConexion();
        boolean ok = false;
        
        Usuario u = new Usuario(nombre, apellidos);
        u.setEmail(email);
        u.setPassword(pw1, pw2);
        
        if (conexion != null && usuario == null) {
            try {
                String insertSQL =
                    "INSERT INTO usuarios(nombre, apellidos, email, password) "
                    + "VALUES (?,?,?,?)";
                PreparedStatement prst = conexion.prepareStatement(insertSQL);
                prst.setString(1, u.getNombre());
                prst.setString(2, u.getApellidos());
                prst.setString(3, u.getEmail());
                prst.setString(4, u.getPassword());
                prst.executeUpdate();
                ok = true;
            } catch (SQLException ex) {
                System.out.println(ex.getMessage() + 
                        ". >>> Error de Insercion de Usuario!!");
            } finally {
                finalizarConexion(conexion);
            } 
        }
        return ok;
    }
    
    public boolean iniciarSesion(String email, String password) {
        Connection conexion = iniciarConexion();
        boolean ok = false;
        
        if (conexion != null && usuario == null) {
            try {
                Statement st = conexion.createStatement();
                
                String selectSQL = "SELECT * FROM usuarios WHERE email='";
                selectSQL += email + "' AND password='" + password + "'";
                
                ResultSet res = st.executeQuery(selectSQL);
                while (res.next()) {
                    String unombre = res.getString("nombre");
                    String uapellidos = res.getString("apellidos");
                    usuario = new Usuario(unombre, uapellidos, email, password);
                    ok = true;
                }
                    
            } catch (SQLException ex) {
                System.out.println(ex.getMessage() + 
                                ". >>> Error de Login de Usuario!!");
            } finally {
                finalizarConexion(conexion);
            }
        } 
        
        return ok;
    }

    public void finalizarSesion() {
        Connection conexion = iniciarConexion();
        if (conexion != null) {
            if (usuario != null) usuario = null;
            finalizarConexion(conexion);
        }
    }
    
    /*
    
    
    
    
    */
    
    public Usuario getUsuario() {
        return usuario;
    }
    
    public Usuario getUsuario(String email) {
        Usuario u = null;
        Connection conexion = iniciarConexion();
        if (conexion != null) {
            try {
                Statement st = conexion.createStatement();
                String selectSQL = 
                    "SELECT * FROM usuarios WHERE email='" + email + "'";
                ResultSet res = st.executeQuery(selectSQL);
                while (res.next()) {
                    String nombre = res.getString("nombre");
                    String apellidos = res.getString("apellidos");
                    String pwd = res.getString("password");
                    
                    u = new Usuario(nombre, apellidos, email, pwd);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage() + 
                        ". >>> Error de Obtencion de Usuario!!");
            } finally {
                finalizarConexion(conexion);
            }
        }
        return u;
    }
    
    public Usuario getMsgDe(String email) {
        Usuario u = null;
        Connection conexion = iniciarConexion();
        if (conexion != null) {
            try {
                try (Statement st = conexion.createStatement()) {
                    String selectSQL =
                            "SELECT * FROM mensajes WHERE de='" + email + "'";
                    try (ResultSet res = st.executeQuery(selectSQL)) {
                        if (res.next()) u = getUsuario(email);
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage() + 
                        ". >>> Error de Obtencion de Usuario!!");
            } finally {
                finalizarConexion(conexion);
            }
        }
        return u;
    }
    
    public Usuario getMsgPara(String email) {
        Usuario u = null;
        Connection conexion = iniciarConexion();
        if (conexion != null) {
            try {
                try (Statement st = conexion.createStatement()) {
                    String selectSQL =
                            "SELECT * FROM mensajes WHERE para='" + email + "'";
                    try (ResultSet res = st.executeQuery(selectSQL)) {
                        if (res.next()) u = getUsuario(email);
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage() + 
                        ". >>> Error de Obtencion de Usuario!!");
            } finally {
                finalizarConexion(conexion);
            }
        }
        return u;
    }
    
    /*
    
   
    
    
    */
    
    public void enviarMensaje(Usuario destino, String texto) {
        Connection conexion = iniciarConexion();
        if (conexion != null && usuario != null) {
            try {
                if (destino != null && 
                        !destino.getEmail().equals(usuario.getEmail())) {
                    String insertSQL = 
                        "INSERT INTO mensajes(de, para, contenido) VALUES (?,?,?)";
                    try (PreparedStatement prst = conexion.prepareStatement(insertSQL)) {
                        prst.setString(1, usuario.getEmail());
                        prst.setString(2, destino.getEmail());
                        prst.setString(3, texto);
                        prst.executeUpdate();
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage() + 
                        ". >>> Error de Insercion de Mensaje!!");
            } finally {
                finalizarConexion(conexion);
            }
        }
    }
    
    public List<Mensaje> verMensajesRecibidos() {
        List<Mensaje> mensajes = new LinkedList<>();
        
        Connection conexion = iniciarConexion();
        if (conexion != null && usuario != null) {
            try {
                try (Statement st = conexion.createStatement()) {
                    String consulta =
                            "SELECT * FROM mensajes WHERE para='" 
                                + usuario.getEmail() + "'";
                    try (ResultSet res = st.executeQuery(consulta)) {
                        while (res.next()) {
                            Usuario u = getMsgDe(res.getString("de"));
                            if (u != null) {
                                String texto = res.getString("contenido");
                                Mensaje msg = new Mensaje(usuario,u);
                                msg.setCuerpo(texto);
                                mensajes.add(msg);
                            }
                        }
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage() + 
                        ". >>> Error de Obtencion de Mensajes Recibidos!!");
            } finally {
                finalizarConexion(conexion);
            }
        }
        return mensajes;
    }
    
    public List<Mensaje> verMensajesEnviados() {
        List<Mensaje> mensajes = new LinkedList<>();
        
        Connection conexion = iniciarConexion();
        if (conexion != null && usuario != null) {
            try {
                try (Statement st = conexion.createStatement()) {
                    String consulta =
                            "SELECT * FROM mensajes WHERE de='" + 
                                usuario.getEmail() + "'";
                    try (ResultSet res = st.executeQuery(consulta)) {
                        while (res.next()) {
                            Usuario u = getMsgPara(res.getString("para"));
                            if (u != null) {
                                String texto = res.getString("contenido");
                                Mensaje msg = new Mensaje(usuario, u);
                                msg.setCuerpo(texto);
                                mensajes.add(msg);
                            }
                        }
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage() + 
                        ". >>> Error de Obtencion de Mensajes Enviados!!");
            } finally {
                finalizarConexion(conexion);
            }
        }
        return mensajes;
    }
}
