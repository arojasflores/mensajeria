<%-- 
    Document   : enviarfail
    Created on : 27-mar-2017, 11:27:59
    Author     : elcer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>   
        <title>Servicio de Mensajer&iacute;a JSP</title> 
    </head>
    <body>
        <%
            String nombre = request.getParameter("nombre");
        %>
        <h1>No se ha podido enviar el mensaje!</h1>
        <h2>
            <%
                int errcod = Integer.parseInt(request.getParameter("errcod"));
                if (errcod == 1) 
                    out.println("No has especificado el email del destinatario.");
                else if (errcod == 2) 
                    out.println("El destinatario con el email especificado no existe en el sistema.");
                else if (errcod == 3)
                    out.println("El cuerpo del mensaje est&aacute; en blanco.");
                else if (errcod == 4)
                    out.println("No puedes enviarte un mensaje a t&iacute; mismo");
            %>
        </h2><br/>
        <p><a href="enviar.jsp?nombre=<%= nombre %>">Intentar de Nuevo</a></p>
        <p><a href="home.jsp?nombre=<%= nombre %>">Cancelar</a></p>
    </body>
</html>
