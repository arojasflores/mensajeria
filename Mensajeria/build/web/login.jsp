<%-- 
    Document   : login
    Created on : 26-mar-2017, 16:44:53
    Author     : elcer
--%>

<%@page import="beans.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="mensajeria" scope="session" class="beans.Servicio" />
        <jsp:setProperty name="mensajeria" property="*" />
        
        <title>Servicio de Mensajer&iacute;a JSP</title>
    </head>
    <body>
        
        <%
            if (request.getParameter("enviar") != null) {
                String email = request.getParameter("email");
                String password = request.getParameter("password");
                
                String redirect;
                if (mensajeria.iniciarSesion(email, password)) {
                    Usuario u = mensajeria.getUsuario();
                    String cnombre = u.getNombre() + " " + u.getApellidos();
                    redirect = "loginok.jsp?nombre=" + cnombre;        
                }
                else redirect = "loginfail.html";
                response.sendRedirect(redirect);   
            }
        %>
        
        <h1>Iniciar Sesi&oacute;n</h1>
        <h2>Por favor, introduce tus datos</h2>
        <form action="login.jsp" method="POST">
            Email: <input type="email" name="email"><br/><br/>
            Clave: <input type="password" name="password"><br/><br/>
            <input type="submit" name="enviar" value="Iniciar Sesi&oacute;n"><br/>
        </form><br/>
        <a href="index.html">Volver atr&aacute;s</a><br/>
    </body>
</html>
