<%-- 
    Document   : enviar
    Created on : 26-mar-2017, 16:16:41
    Author     : elcer
--%>

<%@page import="beans.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="mensajeria" scope="session" class="beans.Servicio" />
        <jsp:setProperty name="mensajeria" property="*" />
        
        <title>Servicio de Mensajer&iacute;a JSP</title>
    </head>
    <body>
        <%
            String nombre = request.getParameter("nombre");
        %>
        <%
            if (request.getParameter("enviar") != null) {
                String redirect;
                Usuario origen = mensajeria.getUsuario();
                nombre = origen.getNombre() + " " + origen.getApellidos();
                
                String email = request.getParameter("email");
                String cuerpo = request.getParameter("cuerpo");
                if (email == "") 
                    redirect = "enviarfail.jsp?errcod=1&nombre=" + nombre;
                else if (cuerpo == "") 
                    redirect = "enviarfail.jsp?errcod=3&nombre=" + nombre;
                else {
                    Usuario destino = mensajeria.getUsuario(email);
                    if (destino == null) 
                        redirect = "enviarfail.jsp?errcod=2&nombre=" + nombre;
                    else if (email.equals(origen.getEmail()))
                        redirect = "enviarfail.jsp?errcod=4&nombre=" + nombre;
                    else {
                        mensajeria.enviarMensaje(destino, cuerpo);
                        redirect = "enviarok.jsp?nombre=" + nombre;
                    }
                }
                response.sendRedirect(redirect);
            }
        %>
        
        <h1>Enviar mensaje</h1>
        <form action="enviar.jsp" method="POST">
            Destinatario: <input type="email" name="email"><br/><br/>
            Cuerpo: <input type="text" name="cuerpo"><br/><br/>
            <input type="submit" name="enviar" value="Enviar"><br/>
        </form><br/>
        <a href="home.jsp?nombre=<%= nombre %>">Cancelar</a><br/>
    </body>
</html>
