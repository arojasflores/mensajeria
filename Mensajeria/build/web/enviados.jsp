<%-- 
    Document   : recibidos
    Created on : 26-mar-2017, 16:18:10
    Author     : elcer
--%>

<%@page import="beans.Mensaje"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="mensajeria" scope="session" class="beans.Servicio" />
        <jsp:setProperty name="mensajeria" property="*" />
        
        <title>Servicio de Mensajer&iacute;a JSP</title>
    </head>
    <body>
        <%
            String nombre = request.getParameter("nombre");
        %>
        <h1>Mensajes Enviados</h1><br/>
        <%
            List<Mensaje> mensajes = mensajeria.verMensajesEnviados();
            if (mensajes.size() == 0) {
        %>
        <h2>No has enviado ning&uacute;n mensaje</h2>
        <%
            } else {
                Iterator<Mensaje> it = mensajes.iterator();
                while (it.hasNext()) {
                    Mensaje m = it.next();
        %>
        <h4>
            <% 
                String origen = m.getOrigen().getNombre();
                origen += " " + m.getOrigen().getApellidos();
                origen += " (" + m.getOrigen().getEmail() + ")";
                out.println("Enviado por: " + origen); 
            %>
        </h4>
        <h4>
            <% 
                String destino = m.getDestino().getNombre();
                destino += " " + m.getDestino().getApellidos();
                destino += " (" + m.getDestino().getEmail() + ")";
                out.println("Enviado a: " + destino); 
            %>
        </h4>
        <p>
            <% 
                String texto = m.getTexto();
                if (texto.length() > 20) {
                    String subtexto = texto.substring(0, 19);
                    out.println("Mensaje: " + subtexto + "...");
                
            %>
            <a href="mensajelargo.jsp?texto=<%= texto %>
               &amp;nombre=<%= nombre %>
               &amp;origen=<%= origen %>
               &amp;destino=<%= destino %>
               &amp;tipo=enviado"> 
                Ver mensaje completo</a><br/>
            <%
                } else
                    out.println(texto); 
            %>
        </p><br/>
        <p>...................................</p>
        <p>...................................</p>
        <%
                }
            }
        %>
        <br/>
        <a href="home.jsp?nombre=<%= nombre %>">Volver atr&aacute;s</a><br/>
    </body>
</html>
