<%-- 
    Document   : loginok
    Created on : 27-mar-2017, 11:57:17
    Author     : elcer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Servicio de Mensajer&iacute;a JSP</title>
    </head>
    <body>
        <%
            String nombre = request.getParameter("nombre");
        %>
        <h1>Te has identificado correctamente!</h1>
        <h2>Hola, <%= nombre %></h2><br/>
        <p><a href="home.jsp?nombre=<%= nombre %>">
                -> Continuar</a></p>
    </body>
</html>
