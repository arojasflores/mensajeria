<%-- 
    Document   : logout
    Created on : 27-mar-2017, 16:25:13
    Author     : elcer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="mensajeria" scope="session" class="beans.Servicio" />
        <jsp:setProperty name="mensajeria" property="*" />
        
        <title>Servicio de Mensajer&iacute;a JSP</title>
    </head>
    <body>
        <%
            mensajeria.finalizarSesion();
        %> 
        <h1>Has cerrado sesi&oacute;n</h1><br/>
        <h2>Hasta luego</h2><br/>
        <p><a href="index.html">-> Continuar</a></p>
    </body>
</html>
