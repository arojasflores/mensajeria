<%-- 
    Document   : register
    Created on : 26-mar-2017, 16:44:43
    Author     : elcer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="mensajeria" scope="session" class="beans.Servicio" />
        <jsp:setProperty name="mensajeria" property="*" />
        
        <title>Servicio de Mensajer&iacute;a JSP</title>
    </head>
    <body>
        
        <%
            if (request.getParameter("enviar") != null) {
                String nombre = request.getParameter("nombre");
                String apellidos = request.getParameter("apellidos");
                String email = request.getParameter("email");
                String pw1 = request.getParameter("pw1");
                String pw2 = request.getParameter("pw2");
                
                String redirect;
                if (mensajeria.registrarse(nombre, apellidos, email, pw1, pw2))
                    redirect = "registerok.html";
                else
                    redirect = "registerfail.html";
                response.sendRedirect(redirect);
            }
        %>
        
        <h1>Registro</h1>
        <h2>Por favor, introduzca sus datos</h2><br/>
        <form action="register.jsp" method="POST">
            Nombre: <input type="text" name="nombre"><br/><br/>
            Apellidos: <input type="text" name="apellidos"><br/><br/>
            Email: <input type="email" name="email"><br/><br/>
            Clave: <input type="password" name="pw1"><br/><br/>
            Repetir clave: <input type="password" name="pw2"><br/><br/>
            <br/>
            <input type="submit" name="enviar" value="Confirmar">
            <input type="reset" name="clear" value="Empezar de Nuevo"><br/><br/>
        </form><br/>
        <a href="index.html">Volver atr&aacute;s</a><br/>
    </body>
</html>
