<%-- 
    Document   : mensajelargo
    Created on : 29-mar-2017, 12:10:01
    Author     : elcer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Servicio de Mensajer&iacute;a JSP</title>
    </head>
    <body>
        <%
            String nombre = request.getParameter("nombre");
            String origen = request.getParameter("origen");
            String destino = request.getParameter("destino");
            String texto = request.getParameter("texto");
            String tipo = request.getParameter("tipo");
        %>
        
        <h1>Mensajes recibidos</h1><br/>
        <h2>Mensaje completo</h2>
        <p>
            <%
                out.println(origen + "<br/><br/>");
                out.println(destino + "<br/><br/>");
                out.println(texto + "<br/><br/>");
                if (tipo.equals("recibido"))
            %>
            <a href="recibidos.jsp?nombre=<%= nombre %>">Volver atr&aacute;s</a><br/> 
            <%
                else if (tipo.equals("enviado"))
            %>
            <a href="enviados.jsp?nombre=<%= nombre %>">Volver atr&aacute;s</a><br/> 
        </p>
    </body>
</html>
