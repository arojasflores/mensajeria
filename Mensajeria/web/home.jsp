<%-- 
    Document   : home.jsp
    Created on : 27-mar-2017, 11:38:14
    Author     : elcer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Servicio de Mensajer&iacute;a JSP</title>
    </head>
    <body>
        <%
            String nombre = request.getParameter("nombre");
        %>
        <p>Hola, <%= nombre %></p><br/>
        <h1>Qu&eacute; quieres hacer?</h1>
        <h2>Men&uacute;</h2>
        <p><a href="enviar.jsp?nombre=<%= nombre %>">
                Enviar mensaje</a></p><br/>
        <p><a href="recibidos.jsp?nombre=<%= nombre %>">
                Ver mensajes recibidos</a></p><br/>
        <p><a href="enviados.jsp?nombre=<%= nombre %>">
                Ver mensajes enviados</a></p><br/>
        <br/>
        <h4><a href="logout.jsp">Salir</a></h4>
    </body>
</html>
